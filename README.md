# README #

This is an example of how to integrate mediQuo SDK in your app. This SDK offers the possibility to include the main functionalities of the mediQuo platform inside your own projects.
See documentation: [https://developer.mediquo.com/docs/introduction/](https://developer.mediquo.com/docs/introduction/)
