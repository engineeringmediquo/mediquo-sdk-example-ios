//
//  Styles.swift
//  mediquo-sdk-example-ios
//
//  Created by David Martin on 22/2/21.
//

import MediQuo_Base
import UIKit

class Styles {
    static var style: MediQuoStyleType { 
        var style = MediQuoStyle()
        // General
        style.prefersLargeTitles = true
        style.primaryColor = UIColor(hex: "#6200EEFF") ?? .black
        style.primaryContrastColor = .white
        style.secondaryColor = UIColor(hex: "#9952FDFF") ?? .black
        style.accentColor = UIColor(hex: "#03DAC5FF") ?? .black
        
        // Chat
        style.messageBackgroundDateColor = .lightGray
        style.chatSubTitleViewColor = .white
        
        // Delegates
        style.professionalsListDelegate = ListDelegate()
        
        // Fonts
        style.bookFont = UIFontHelper.getStandarUIFont(from: .book, and: 16)
        style.mediumFont = UIFontHelper.getStandarUIFont(from: .medium, and: 16)
        style.boldFont = UIFontHelper.getStandarUIFont(from: .bold, and: 16) 
        
        return style
    }
}

class ListDelegate: ProfessionalsListDelegate {
    var viewController: BaseViewControllerSDK?

    func getRightBarButton() -> UIBarButtonItem {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "3dots")
        let option = UIBarButtonItem(image: imageView.image, style: .plain, target: self, action: #selector(self.showOptions))
        option.tintColor = .white
        return option
    }

    func onListLoaded() {
        print("[🚀🚀🚀] List is loaded!")
    }

    func onProfessionalClick(professionalId: Int, specialityId: Int, hasAccess: Bool) -> Bool {
        print("[🚀🚀🚀] \(professionalId) \(specialityId) \(hasAccess)")
        return true
    }

    func onUnreadMessage(countChange: Int) {
        print("[🚀🚀🚀] New unread messages count: \(countChange)")
    }
}

extension ListDelegate {
    @objc private func showOptions() {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Mensajes sin leer", style: .default, handler: { [weak self] _ in
            guard let self = self else { return }
            self.getPendingMessages()
        }))
        
        alert.addAction(UIAlertAction(title: "Historial médico", style: .default, handler: { [weak self] _ in
            guard let self = self else { return }
            self.navigateToMedicalHistory()
        }))
        
        alert.addAction(UIAlertAction(title: "Alergias", style: .default, handler: { [weak self] _ in
            guard let self = self else { return }
            self.navigateToAllergies()
        }))
        
        alert.addAction(UIAlertAction(title: "Enfermedades", style: .default, handler: { [weak self] _ in
            guard let self = self else { return }
            self.navigateToDiseases()
        }))
        
        alert.addAction(UIAlertAction(title: "Medicaciones", style: .default, handler: { [weak self] _ in
            guard let self = self else { return }
            self.navigateToMedications()
        }))
        
        alert.addAction(UIAlertAction(title: "Informes", style: .default, handler: { [weak self] _ in
            guard let self = self else { return }
            self.navigateToReports()
        }))
        
        alert.addAction(UIAlertAction(title: "Recetas", style: .default, handler: { [weak self] _ in
            guard let self = self else { return }
            self.navigateToRecipes()
        }))
        
        alert.addAction(UIAlertAction(title: "Logout", style: .default, handler: { [weak self] _ in
            guard let self = self else { return }
            self.logout()
        }))

        alert.addAction(UIAlertAction(title: NSLocalizedString("Cerrar", comment: ""), style: .cancel))
        self.viewController?.present(alert, animated: true)
    }

    private func getPendingMessages() {
        MediQuoSDK.getPendingMessages { [weak self] count in
            guard let self = self else { return }
            let alert = UIAlertController(title: "Mensajes sin leer", message: "Tienes \(count) mensajes sin leer", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Cerrar", style: .cancel, handler: nil))
            self.viewController?.present(alert, animated: true)
        }
    }

    private func navigateToMedicalHistory() {
        let viewController = MediQuoSDK.getMedicalHistory() 
        let navigationController = UINavigationController(rootViewController: viewController)
        self.viewController?.present(navigationController, animated: true)
    }

    private func navigateToAllergies() {
        let viewController = MediQuoSDK.getAllergies()
        let navigationController = UINavigationController(rootViewController: viewController)
        self.viewController?.present(navigationController, animated: true)
    }

    private func navigateToDiseases() {
        let viewController = MediQuoSDK.getDiseases()
        let navigationController = UINavigationController(rootViewController: viewController)
        self.viewController?.present(navigationController, animated: true)
    }

    private func navigateToMedications() {
        let viewController = MediQuoSDK.getMedications()
        let navigationController = UINavigationController(rootViewController: viewController)
        self.viewController?.present(navigationController, animated: true)
    }

    private func navigateToReports() {
        let viewController = MediQuoSDK.getReports()
        let navigationController = UINavigationController(rootViewController: viewController)
        self.viewController?.present(navigationController, animated: true)
    }

    private func navigateToRecipes() {
        let viewController = MediQuoSDK.getRecipes()
        let navigationController = UINavigationController(rootViewController: viewController)
        self.viewController?.present(navigationController, animated: true)
    }

    private func logout() {
        MediQuoSDK.deauthenticate { [weak self] result in
            if result {
                CoreLog.firebase.info("Deauthentication service was finished successfully")
            } else {
                CoreLog.firebase.error("Deauthentication service couldn't finished successfully")
            }
            SessionStorage.clear()
            guard let viewController: LoginViewController =
                    UIStoryboard(name: "Login", bundle: nil).instantiateInitialViewController() as? LoginViewController else { return }
            viewController.modalPresentationStyle = .overFullScreen
            self?.viewController?.navigationController?.present(viewController, animated: true)
        }
    }
}
