//
//  LoginViewController.swift
//  mediquo-sdk-example-ios
//
//  Created by David Martin on 7/12/21.
//

import MediQuo_Base
import PromiseKit
import UIKit

class LoginViewController: UIViewController {
    @IBOutlet private var apiKeyTextField: UITextField! {
        didSet {
            self.apiKeyTextField.text = API_KEY
        }
    }

    @IBOutlet private var codeTextField: UITextField! {
        didSet {
            self.codeTextField.text = CLIENT_CODE
        }
    }

    @IBOutlet private var environment: UISwitch!

    @IBOutlet private var actionButton: UIButton! {
        didSet {
            self.actionButton.layer.cornerRadius = 8
            self.actionButton.setTitleColor(.white, for: .normal)
        }
    }

    @IBAction private func didTapActionButton() {
        self.enableDisableActionButton(isEnabled: false)
        let viewModel = LoginViewModel(apiKey: self.apiKeyTextField.text, code: self.codeTextField.text, environment: self.environment.isOn)
        viewModel.doAction { [weak self] result in
            guard let self = self else { return }
            switch result {
                case .success:
                    self.navigate()
                case let .failure(error):
                    self.showError(message: error.localizedDescription)
                    self.enableDisableActionButton(isEnabled: true)
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
        view.addGestureRecognizer(tapGesture)
    }
    
    @objc private func hideKeyboard() {
        apiKeyTextField.resignFirstResponder()
        codeTextField.resignFirstResponder()
    }

    private func navigate() {
        guard let viewController: UINavigationController = UIStoryboard(name: "Main", bundle: nil).instantiateInitialViewController() as? UINavigationController else {
            return
        }
        hideKeyboard()
        viewController.modalPresentationStyle = .overFullScreen
        self.present(viewController, animated: true)
    }

    private func showError(message: String) {
        let alert = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Close", style: .cancel))
        self.present(alert, animated: true)
    }

    private func enableDisableActionButton(isEnabled: Bool) {
        self.actionButton.isEnabled = isEnabled
    }
}
