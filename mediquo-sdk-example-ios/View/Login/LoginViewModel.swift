//
//  LoginViewModel.swift
//  mediquo-sdk-example-ios
//
//  Created by David Martin on 9/12/21.
//

import MediQuo_Base

class LoginViewModel {
    private var apiKeyValue: String?
    private var codeValue: String?
    private var environmentValue: Bool?

    init(apiKey: String?, code: String?, environment: Bool?) {
        self.apiKeyValue = apiKey
        self.codeValue = code
        self.environmentValue = environment
    }

    func doAction(completion: @escaping (Swift.Result<Void, Error>) -> Void) {
        guard let apiKey = self.apiKeyValue, !apiKey.isEmpty, let code = self.codeValue, !code.isEmpty else {
            completion(.failure(BaseError.repositoryError(.reason("No values found"))))
            return
        }
        let manager = BusinessManager(apiKey: apiKey, code: code, environment: self.isProduction())
        manager.doAction { result in
            switch result {
            case .success:
                SessionStorage.set(SessionModel(apiKey: apiKey, code: code, isProd: self.isProduction()))
                completion(.success(()))
            case let .failure(error):
                CoreLog.business.error("%@", error.localizedDescription)
                completion(.failure(error))
            }
        }
    }

    private func isProduction() -> Bool {
        guard let environment = self.environmentValue, environment else { return false }
        return true
    }
}
