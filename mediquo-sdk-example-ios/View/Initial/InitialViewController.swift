//
//  InitialViewController.swift
//  mediquo-sdk-example-ios
//
//  Created by David Martin on 9/12/21.
//

import MediQuo_Base
import UIKit

class InitialViewController: UIViewController {
    private let viewModel = InitialViewModel()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.doAction()
    }

    private func doAction() {
        self.viewModel.doAction(onEnterInApp: { [weak self] in
            self?.enterInApp()
        }, onEnterInLogin: { [weak self] in
            self?.enterInLogin()
        })
    }

    private func enterInApp() {
        guard let viewController: UINavigationController =
                UIStoryboard(name: "Main", bundle: nil).instantiateInitialViewController() as? UINavigationController else {
            fatalError("Can't authenticate")
        }
        viewController.modalPresentationStyle = .overFullScreen
        self.present(viewController, animated: false)
    }

    private func enterInLogin() {
        guard let viewController: LoginViewController = UIStoryboard(name: "Login", bundle: nil).instantiateInitialViewController() as? LoginViewController else {
            fatalError("Can't authenticate")
        }
        viewController.modalPresentationStyle = .overFullScreen
        self.navigationController?.present(viewController, animated: false)
    }
}
