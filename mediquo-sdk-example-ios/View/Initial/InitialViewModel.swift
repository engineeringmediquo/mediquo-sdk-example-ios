//
//  InitialViewModel.swift
//  mediquo-sdk-example-ios
//
//  Created by David Martin on 9/12/21.
//

import MediQuo_Base

class InitialViewModel {
    func doAction(onEnterInApp: @escaping () -> Void, onEnterInLogin: @escaping () -> Void) {
        if let model = SessionStorage.get() {
            let manager = BusinessManager(apiKey: model.apiKey, code: model.code, environment: model.isProd)
            manager.doAction { result in
                switch result {
                case .success:
                    onEnterInApp()
                case .failure:
                    onEnterInLogin()
                }
            }
        } else {
            onEnterInLogin()
        }
    }
}
