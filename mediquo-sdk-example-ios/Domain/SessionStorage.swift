//
//  SessionStorage.swift
//  mediquo-sdk-example-ios
//
//  Created by David Martin on 9/12/21.
//

import MediQuo_Base

class SessionStorage {
    private static let key: String = String(describing: SessionStorage.self)

    static func set(_ model: SessionModel?) {
        guard let newValue = model, let value = try? JSONEncoder().encode(newValue) else {
            UserDefaults.standard.removeObject(forKey: key)
            return
        }
        UserDefaults.standard.set(value, forKey: key)
    }

    static func get() -> SessionModel? {
        guard let data = UserDefaults.standard.object(forKey: key) as? Data,
              let value = try? JSONDecoder().decode(SessionModel.self, from: data) else { return nil }
        return value
    }

    static func clear() {
        UserDefaults.standard.removeObject(forKey: key)
    }
}

struct SessionModel: Codable {
    var apiKey: String
    var code: String
    var isProd: Bool
}
