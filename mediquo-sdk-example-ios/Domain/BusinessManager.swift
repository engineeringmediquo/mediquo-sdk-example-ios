//
//  BusinessManager.swift
//  mediquo-sdk-example-ios
//
//  Created by David Martin on 9/12/21.
//

import MediQuo_Base
import PromiseKit

class BusinessManager {
    private var apiKeyValue: String?
    private var codeValue: String?
    private var environmentValue: Bool?

    init(apiKey: String?, code: String?, environment: Bool?) {
        self.apiKeyValue = apiKey
        self.codeValue = code
        self.environmentValue = environment
    }

    func doAction(completion: @escaping (Swift.Result<Void, Error>) -> Void) {
        firstly {
            self.initialize()
        }.then {
            self.authenticate()
        }.done {
            completion(.success(()))
        }.catch { error in
            CoreLog.business.error("%@", error.localizedDescription)
            completion(.failure(error))
        }
    }

    private func isProduction() -> Bool {
        guard let environment = self.environmentValue, environment else { return false }
        return true
    }

    private func initialize() -> Promise<Void> {
        return Promise<Void> { seal in
            guard let apiKey = self.apiKeyValue else {
                seal.reject(BaseError.repositoryError(.reason("No apiKey found")))
                return
            }
            if self.isProduction() {
                MediQuoSDK.initialize(apiKey: apiKey, style: Styles.style) { result in
                    if case .success = result {
                        CoreLog.business.info("Initialize has been done successfully")
                        seal.fulfill_()
                    }
                    if case let .failure(error) = result {
                        CoreLog.business.info("Initialize has errors %@", error.description)
                        seal.reject(error)
                    }
                }
            } else {
                MediQuoSDK._mqInit(apiKey: apiKey, style: Styles.style) { result in
                    if case .success = result {
                        CoreLog.business.info("Initialize has been done successfully")
                        seal.fulfill_()
                    }
                    if case let .failure(error) = result {
                        CoreLog.business.info("Initialize has errors %@", error.description)
                        seal.reject(error)
                    }
                }
            }
        }
    }

    private func authenticate() -> Promise<Void> {
        return Promise<Void> { seal in
            guard let apiKey = self.apiKeyValue, let code = self.codeValue, let isProd = self.environmentValue else {
                seal.reject(BaseError.repositoryError(.reason("No code found")))
                return
            }
            MediQuoSDK.authenticate(clientCode: code) { status in
                guard case SdkStatus.ready = status else {
                    seal.reject(BaseError.repositoryError(.reason("Can't authenticate")))
                    return
                }
                SessionStorage.set(SessionModel(apiKey: apiKey, code: code, isProd: isProd))
                seal.fulfill_()
            }
        }
    }
}
