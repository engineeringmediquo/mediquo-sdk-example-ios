//
//  AppDelegate.swift
//  mediquo-sdk-example-ios
//
//  Created by David Martin on 20/2/21.
//

// TODO: Cane - 🤷🏻‍♂️ El pod de Firebase está dando problemas al crear el archive
//import Firebase
//import FirebaseMessaging
import MediQuo_Base
import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
//        FirebaseApp.configure()
//        Messaging.messaging().delegate = self
        UNUserNotificationCenter.current().delegate = self

        guard let initialViewController: UINavigationController =
                UIStoryboard(name: "Initial", bundle: nil).instantiateInitialViewController() as? UINavigationController else {
            return false
        }

        self.window = UIWindow(frame: UIScreen.main.bounds)
        self.window?.rootViewController = initialViewController
        self.window?.makeKeyAndVisible()
        return true
    }
}

//extension AppDelegate: MessagingDelegate {
//    func messaging(_: Messaging, didReceiveRegistrationToken fcmToken: String?) {
//        guard let fcmToken = fcmToken else { return }
//        MediQuoSDK.registerFirebase(token: fcmToken) { result in
//            if result {
//                CoreLog.firebase.info("Firebase registration token: %@", fcmToken)
//            } else {
//                CoreLog.firebase.error("Can't register Firebase registration token")
//            }
//        }
//    }
//}

extension AppDelegate {
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
//        Messaging.messaging().apnsToken = deviceToken
//        if let fcmToken = Messaging.messaging().fcmToken {
//            CoreLog.firebase.error("didRegisterForRemoteNotificationsWithDeviceToken: %@", fcmToken)
//            MediQuoSDK.registerFirebase(token: fcmToken) { result in
//                if result {
//                    CoreLog.firebase.info("Firebase registration token: %@", fcmToken)
//                } else {
//                    CoreLog.firebase.error("Can't register Firebase registration token")
//                }
//            }
//        }
    }

    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        CoreLog.firebase.error("didFailToRegisterForRemoteNotificationsWithError: %@", error.localizedDescription)
    }

    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        MediQuoSDK.didReceiveRemoteNotification(userInfo: userInfo, completion: completionHandler)
    }
}

extension AppDelegate: UNUserNotificationCenterDelegate {
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        MediQuoSDK.didReceiveRemoteNotification(userInfo: userInfo, completion: completionHandler)
    }

    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo
        MediQuoSDK.willPresentRemoteNotification(userInfo: userInfo, completion: completionHandler)
    }
}
